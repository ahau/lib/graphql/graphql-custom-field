const validBlobScuttlebutt = {
  type: 'ssb',
  blobId: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
  mimeType: 'application/pdf',
  size: 12345,
  unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'
}

const validBlobHyper = {
  type: 'hyper',
  driveAddress: '6LJVf5C5T1WauuJUgCWUA0mT+ak9pV5fFH4uhw2PeVU=',
  blobId: '2df0cb34-77e5-40af-99ad-52d85ff67d35',
  readKey: 'Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=',
  mimeType: 'application/pdf',
  size: 12345
}

const invalidBlobScuttlebutt = {
  type: 'ssb',
  // blobId: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
  mimeType: 'application/pdf',
  size: 12345,
  unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'
}

const invalidBlobHyper = {
  type: 'hyper',
  driveAddress: '6LJVf5C5T1WauuJUgCWUA0mT+ak9pV5fFH4uhw2PeVU=',
  // blobId: '2df0cb34-77e5-40af-99ad-52d85ff67d35',
  readKey: 'Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=',
  mimeType: 'application/pdf',
  size: 12345
}

module.exports = {
  validBlobHyper,
  validBlobScuttlebutt,
  invalidBlobHyper,
  invalidBlobScuttlebutt
}
