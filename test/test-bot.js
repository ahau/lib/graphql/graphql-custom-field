import AhauClient from 'ahau-graphql-client'

const ahauServer = require('ahau-graphql-server')
const gql = require('graphql-tag')
const fetch = require('node-fetch')
const CustomField = require('../')

const {
  validBlobHyper,
  validBlobScuttlebutt,
  invalidBlobHyper,
  invalidBlobScuttlebutt
} = require('./lib/mocks')

function resolver (input) {
  return input
}

module.exports = async function () {
  const resolvers = {

    CustomField,

    Query: {
      // empty values
      nullValue: () => null,
      emptyString: () => '',
      emptyArray: () => [],

      // non-empty values
      validString: () => 'dog',
      validStringArray: () => ['dog', 'cat'],

      // boolean
      validBooleanTrue: () => true,
      validBooleanFalse: () => false,

      // number
      validInt: () => 1,
      validFloat: () => 0.1,

      // blobs
      validBlobScuttlebutt: () => validBlobScuttlebutt,
      validBlobHyper: () => validBlobHyper,
      validBlobArray: () => [
        validBlobScuttlebutt,
        validBlobHyper
      ],

      invalidArray1: () => [true],
      invalidArray2: () => ['dog', 1],
      invalidObject: () => ({ dog: 'hi' }),

      invalidBlobScuttlebutt: () => invalidBlobScuttlebutt,
      invalidBlobHyper: () => invalidBlobHyper,
      invalidBlobArray: () => [
        validBlobScuttlebutt,
        invalidBlobScuttlebutt
      ]
    },
    Mutation: {
      saveField: (_, { input }) => resolver(input)
    }
  }

  const typeDefs = gql`
    scalar CustomField

    type Query {
      nullValue: CustomField
      emptyString: CustomField
      emptyArray: CustomField

      validString: CustomField
      validStringArray: CustomField

      validBooleanTrue: CustomField
      validBooleanFalse: CustomField

      validInt: CustomField
      validFloat: CustomField

      validBlobScuttlebutt: CustomField
      validBlobHyper: CustomField
      validBlobArray: CustomField

      # invalid cases
      invalidArray1: CustomField
      invalidArray2: CustomField
      invalidObject: CustomField
      invalidBlobScuttlebutt: CustomField
      invalidBlobHyper: CustomField
      invalidBlobArray: CustomField
    }

    type Mutation {
      saveField(input: CustomField): CustomField
    }
  `

  const port = 34455

  const server = await ahauServer({
    context: {},
    schemas: [{ typeDefs, resolvers }],
    port
  })

  const apollo = new AhauClient(port, { fetch, isTesting: true })

  return {
    server,
    apollo
  }
}
