# graphql-custom-field

Use JavaScript CustomField for fields that can have values of multiple types.

E.g. Can be used if you want to return data which has could have different forms:

```js
// res.data.Profile =>
{
  name: 'Cherese',
  additionalFields: [
    {
      key: 'yearOfBirth',
      value: 1999                  // Int
    },
    {
      key: 'nicknames',
      value: ['Reese', 'Cherry']   // [String]
    }
  ]
}
```

CustomField allows you to do this easily:

```js
type Profile {
  name: String
  additionalFields {
    key: String
    value: CustomField  // String | [String] | Int | Float | Boolean | null
  }
}
```

> NOTE: Can be used in the way for an input types

Supported Types:

- `String`
- `[String]`
- `Int`
- `Float`
- `Boolean`

## Installation

```
npm i --save graphql-custom-field
```

## Usage

Resolvers
```js
const CustomField = require('graphql-custom-field')

const resolvers = {
  CustomField,

  Query: {
    getProfile // ...
  },
  Mutation: {
    saveProfile // ...
  }
}


```

TypeDefs
```js
const { gql } = require('apollo-server')

module.exports = gql`
  # Define scalar type
  scalar CustomField

  type Query {
    getProfile(id: String): Profile
  }

  type Profile {
    id: String
    name: String
    additionalFields: [Field]
  }

  type Field {
    key: ID!
    value: CustomField
  }

  type Mutation {
    saveProfile(input: ProfileInput): Profile
  }

  input ProfileInput {
    id: String
    name: String
    additionalFields: [FieldInput]
  }

  input FieldInput {
    key: String
    value: CustomField
  }
```
