const { Kind } = require('graphql/language')
const test = require('tape')
const gql = require('graphql-tag')

const CustomField = require('../index.js')
const TestBot = require('./test-bot')
const { RunMutation, RunQuery } = require('./lib/helpers.js')
const {
  validBlobHyper,
  validBlobScuttlebutt,
  invalidBlobHyper,
  invalidBlobScuttlebutt
} = require('./lib/mocks')

test('custom field scalar', async t => {
  const { apollo, server } = await TestBot()
  t.plan(138)

  const runQuery = RunQuery(apollo, t)
  const runMutation = RunMutation(apollo, t)

  const runNormalMutation = async (input, hasError) => {
    return runMutation(input, {
      mutation: gql`
        mutation($input: CustomField) {
          saveField(input: $input)
        }
      `,
      variables: { input }
    }, hasError)
  }

  const runLiteralMutation = async (input, hasError) => {
    input = formatInput(input)

    return runMutation(input, {
      mutation: gql`
        mutation {
          saveField(input: "${input}")
        }
      `
    }, hasError)
  }

  // NOTE: this mirrors the api provided by test-bot.js

  // ///
  // valid
  // ///
  const validCases = {
    nullValue: null,
    emptyString: '',
    emptyArray: [],

    validString: 'dog',
    validStringArray: ['dog', 'cat'],

    validBooleanTrue: true,
    validBooleanFalse: false,

    validInt: 1,
    validFloat: 0.1,
    validBlobScuttlebutt,
    validBlobHyper,
    validBlobArray: [
      validBlobScuttlebutt,
      validBlobHyper
    ]
  }

  await Promise.all(
    Object.entries(validCases).map(async ([field, value]) => {
      const strValue = JSON.stringify(value)

      t.deepEquals(
        await runQuery(field),
        value,
        `query accepts ${strValue} value`
      )

      t.deepEquals(
        await runNormalMutation(value),
        value,
        `mutation accepts ${strValue}`
      )

      t.deepEquals(
        await runLiteralMutation(value),
        value,
        `mutation (literal) accepts ${strValue}`
      )

      t.doesNotThrow(
        () => CustomField.serialize(value),
        `CustomField.serialize(${strValue}) does not throw an error`
      )

      t.doesNotThrow(
        () => CustomField.parseValue(value),
        `CustomField.parseValue(${strValue}) does not throw an error`
      )

      t.doesNotThrow(
        () => CustomField.parseLiteral({ value, kind: Kind.STRING }),
        `CustomField.parseLiteral(${strValue}) does not throw an error`
      )
    })
  )

  // ///
  // invalid
  // ///
  const invalidCases = {
    invalidArray1: [true],
    invalidArray2: ['dog', 1],
    invalidObject: ({ dog: 'hi' }),
    invalidBlobScuttlebutt,
    invalidBlobHyper
  }

  // NOTE: we provide the expected error for some cases as graphql changes the string format
  // for objects and arrays
  const expectedErrors = {
    invalidBlobScuttlebutt: 'Variable "$input" got invalid value { type: "ssb", mimeType: "application/pdf", size: 12345, unbox: "YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs" }; Value is not of type String, [String], Boolean, BlobScuttlebutt, BlobHyper or null. Got {"type":"ssb","mimeType":"application/pdf","size":12345,"unbox":"YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs"} instead',
    invalidBlobHyper: 'Variable "$input" got invalid value { type: "hyper", driveAddress: "6LJVf5C5T1WauuJUgCWUA0mT+ak9pV5fFH4uhw2PeVU=", readKey: "Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=", mimeType: "application/pdf", size: 12345 }; Value is not of type String, [String], Boolean, BlobScuttlebutt, BlobHyper or null. Got {"type":"hyper","driveAddress":"6LJVf5C5T1WauuJUgCWUA0mT+ak9pV5fFH4uhw2PeVU=","readKey":"Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=","mimeType":"application/pdf","size":12345} instead'
  }

  const runInvalidQuery = (field) => runQuery(field, true)
  const runInvalidMutation = (input) => runNormalMutation(input, false)
  const runInvalidLiteralMutation = (input) => runLiteralMutation(input, true)

  await Promise.all(
    Object.entries(invalidCases).map(async ([field, value]) => {
      const strValue = JSON.stringify(value)
      const formattedValue = formatValue(value)

      await runInvalidQuery(field)
        .then(res => t.notOk(res, 'should not be here'))
        .catch(err => {
          t.equals(
            err.message,
            `Value is not of type String, [String], Boolean, BlobScuttlebutt, BlobHyper or null. Got ${strValue} instead`,
            `${field} invalid value ${strValue} returns error (1)`
          )
        })

      await runInvalidMutation(value)
        .then(res => t.notOk(res, 'should not be here'))
        .catch(err => {
          t.equals(
            err.networkError.result.errors[0].message,
            expectedErrors[field] || `Variable "$input" got invalid value ${formattedValue}; Value is not of type String, [String], Boolean, BlobScuttlebutt, BlobHyper or null. Got ${strValue} instead`,
            `${field} invalid value ${strValue} returns error (2)`
          )
        })

      await runInvalidLiteralMutation(value)
        .then(res => t.notOk(res, 'should not be here'))
        .catch(err => t.ok(err, `invalid value ${strValue} returns error (3)`))

      t.throws(
        () => CustomField.serialize(value),
        `CustomField.serialize(${strValue}) throws an error`
      )

      t.throws(
        () => CustomField.parseValue(value),
        `CustomField.parseValue(${strValue}) throws an error`
      )

      t.throws(
        () => CustomField.parseLiteral({ value, kind: Kind.STRING }),
        `CustomField.parseLiteral((${strValue}) throws an error`
      )
    })
  )

  server.close()
})

// need to make sure the input is given correctly as it is converted to a string
// when passed to the parseLiteral method
function formatInput (input) {
  if (typeof input === 'object') {
    input = JSON.stringify(input)
    if (Array.isArray(input) || input.length > 0) {
      input = input.replace(/"/g, "'")
    }
  } else {
    input = input.toString()
  }

  return input
}

function formatValue (val) {
  return (
    JSON.stringify(val)
      .replace(',', ', ')
      .replace('{"dog":"hi"}', '{ dog: "hi" }') // hack as graphql error message prints differently
  )
}
