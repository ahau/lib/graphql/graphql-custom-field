const { Kind, GraphQLScalarType, GraphQLError } = require('graphql')

const isBlob = require('./lib/is-blob')

module.exports = new GraphQLScalarType({
  name: 'CustomField',

  description: 'Use JavaScript CustomField for fields that can be multiple values.',

  serialize (value) {
    return parseValue(value)
  },
  parseValue,
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) return null

    let v = ast.value

    try {
      if (typeof v === 'string') {
        v = v.replace(/'/g, '"')
        v = JSON.parse(v)
      }
    } catch (err) {
      // console.log(`CustomField.parseLiteral JSON.parse(${JSON.stringify(v)}) threw an error. Ignoring for now.`)
      // console.log(err)
      // fail gracefully, because most input that comes in will be a string,
      // JSON.parse will fail on strings like "dog". It is mainly used to convert strings to
      // objects or arrays for the parse method to check the value is in the correct format
    }

    parseValue(v)

    return v
  }
})

function parseValue (value) {
  if (value === null) return null
  parse(value)
  return value
}

function parse (value) {
  if (value === null) return

  switch (typeof value) {
    case 'boolean': return
    case 'string': return
    case 'number': return
    case 'object':
      if (isStringArray(value)) return
      if (isBlob(value)) return
  }

  throw new GraphQLError(
    `Value is not of type String, [String], Boolean, BlobScuttlebutt, BlobHyper or null. Got ${JSON.stringify(value)} instead`
  )
}

function isStringArray (v) {
  return (
    Array.isArray(v) &&
    (v.every(i => typeof i === 'string'))
  )
}
