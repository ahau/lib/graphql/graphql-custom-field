const Validator = require('is-my-json-valid')
/*
Support for ssb-blobs and ssb-hyper-blobs, example typeDefs taken from:

https://gitlab.com/ahau/lib/graphql/ssb-graphql-main/-/blob/master/src/typeDefs.js

enum BlobType {
  ssb
  hyper
}

"""
binary file storage
"""
interface Blob {
  type: BlobType!
  mimeType: String!
  size: Int!
  blobId: ID!
  uri: String
}

"""
Blobs of the ssb-blobs variety
"""
type BlobScuttlebutt implements Blob {
  type: BlobType!
  mimeType: String!
  size: Int!
  blobId: ID!
  uri: String

  unbox: String!
}

"""
Blobs of the ssb-hyper-blobs variety
"""
type BlobHyper implements Blob {
  type: BlobType!
  mimeType: String!
  size: Int!
  blobId: ID!
  uri: String

  driveAddress: String!
  readKey: String!
}

*/

const string = { type: 'string' }
const integer = { type: 'integer', minimum: 0 }

const blobScuttlebuttSchema = {
  type: 'object',
  properties: {
    type: { ...string, pattern: '^ssb$' },
    mimeType: string,
    size: integer,
    blobId: string,
    uri: string,
    unbox: string
  },
  required: ['type', 'mimeType', 'size', 'blobId', 'unbox'],
  additionalProperties: false
}

const blobHyperSchema = {
  type: 'object',
  properties: {
    type: { ...string, pattern: '^hyper$' },
    mimeType: string,
    size: integer,
    blobId: string,
    uri: string,
    driveAddress: string,
    readKey: string
  },
  required: ['type', 'mimeType', 'size', 'blobId', 'driveAddress', 'readKey'],
  additionalProperties: false
}

// allow an array of multiple blobs, of both blob types
const blobArraySchema = {
  type: 'array',
  items: {
    anyOf: [
      blobScuttlebuttSchema,
      blobHyperSchema
    ]
  },
  additionalProperties: false
}

module.exports = function isValidBlob (value) {
  return Validator({
    oneOf: [
      blobScuttlebuttSchema,
      blobHyperSchema,
      blobArraySchema
    ]
  })(value)
}
