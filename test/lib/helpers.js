const gql = require('graphql-tag')

function RunQuery (apollo, t) {
  return async function runQuery (queryName, hasError) {
    const res = await apollo.query({
      query: gql`
        query {
          ${queryName}
        }
      `
    })

    if (hasError) return res.errors

    t.error(res.errors, `${queryName} has no errors`)

    return res.data[queryName]
  }
}

function RunMutation (apollo, t) {
  return async function runMutation (input, mutation, hasError) {
    const res = await apollo.mutate(mutation)

    if (hasError) return res.errors

    t.error(res.errors, 'doesnt throw an error on input ' + JSON.stringify(input))

    return res.data.saveField
  }
}

module.exports = {
  RunQuery,
  RunMutation
}
